# Guides pédagogiques

Deux guides pédagogiques du SNDS sont publiés par la Cnam sous licence MPL-2.0, 
pour les [accès permanents](../../files/Cnam/2019-07_Cnam_Guide_pedagogique_SNDS_acces_permanents_MPL-2.0.docx) et pour les [accès sur projet](../../files/Cnam/2019-07_Cnam_Guide_pedagogique_SNDS_acces_projet_MPL-2.0.docx).  
