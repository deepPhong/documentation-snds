# Ressources
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section rassemble des ressources disponibles sur le SNDS.

## Documents téléchargeables

Les pages suivantes référencent les documents publiés sur ce site par chaque organisme : 
[Cnam](documents%20Cnam/README.md), 
[Santé publique France](Sante_publique_France.md),
et [GIS Epi-Phare](Epi-Phare.md).

::: tip Note
Les fiches thématiques aux formats `pdf` ou `word` ne sont pas listées ici, mais référencées directement dans les fiches de documentations correspondantes.

Se référer à ce [dossier sur GitLab](https://gitlab.com/healthdatahub/documentation-snds/tree/master/files) 
pour lister l'ensemble des documents **et** fiches publiés par chaque organisme.  
:::

## Outils en ligne

- Le [forum d'entraide](https://entraide.health-data-hub.fr) de la communauté des utilisateurs du SNDS.
- Un [dictionnaire interactif](https://drees.shinyapps.io/dico-snds/) du SNDS, produit par la DREES.
- Un [schema formalisé du SNDS](https://gitlab.com/healthdatahub/schema-snds), 
qui alimente le dictionnaire interactif, et la partie **Tables** de cette documentation.
- Une [cartographie des indicateurs de santé](http://dataviz.drees.solidarites-sante.gouv.fr/indicateurs_de_sante/) produite par la DREES, décrite [ici](cartographie_indicateurs.md).

## Autre ressources  

- Ressources sur le SNDS [disponibles ailleurs en ligne](internet.md)
- [Présentations et vidéos](meetup.md) des meetup SNDS
- Référencement de [programmes](programmes.md) dont le code est public
- Guide pour [accéder à la documentation du portail SNIIRAM](portail_sniiram.md)
- [Bibliographie](bibliographie.md)
