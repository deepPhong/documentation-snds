# EB_CAM_F

Table des données de codage de la Classification Commune des Actes Médicaux


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|CAM_REM_BSE|nombre réel|Base de remboursement de la CCAM|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|CAM_DOC_EXT|chaîne de caractères|Extension documentaire|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|CAM_QUA_DEN|chaîne de caractères|Localisation dentaire|||
|CAM_ASS_COD|chaîne de caractères|Code association|||
|CAM_MOD_COD|chaîne de caractères|Codes modificateurs|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|CAM_TRT_PHA|nombre réel|Phase de traitement|||
|CAM_ACT_COD|chaîne de caractères|Code activite|||
|CAM_ORD_NUM|nombre réel|Numéro d'ordre de la prestation affinée CCAM|||
|CAM_PRS_IDE|chaîne de caractères|Code CCAM de l'acte médical|||
|CAM_GRI_TAR|chaîne de caractères|Grille tarifaire|||
|CAM_REM_COD|chaîne de caractères|Code remboursement exceptionnel|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|CAM_ACT_PRU|nombre réel|Prix unitaire CCAM de l'acte médical|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
