# EB_UCD_F

Table des données de codage des Unités Communes de Dispensation


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|UCD_ACH_PRU|nombre réel|Prix d'achat unitaire|||
|UCD_FAC_PRU|nombre réel|Prix unitaire facturé|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|UCD_TTF_MNT|nombre réel|Montant total TTC facturé|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|UCD_UCD_COD|chaîne de caractères|Code UCD de la pharmacie hospitalière|||
|UCD_CAL_PRU|nombre réel|Prix unitaire calculé|||
|UCD_TOP_UCD|nombre réel|Top UCD|||
|UCD_ORD_NUM|nombre réel|Numéro d'Ordre de la Prestation Affinée UCD|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|UCD_RCT_COU|nombre réel|Coût de reconstitution du médicament|||
|UCD_ECT_MNT|nombre réel|Montant UCD total de l'écart indemnisable|||
|UCD_ECU_MNT|nombre réel|Montant UCD unitaire de l'écart indemnisable|||
|UCD_FRC_COE|nombre réel|Coefficient de fractionnement|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|UCD_DLV_NBR|nombre réel|Nombre d'unités délivrées|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|UCD_MAR_MNT|nombre réel|Montant UCD TTC de la marge de rétrocession|||
|UCD_LGN_NUM|nombre réel|Numéro de ligne UCD|||
