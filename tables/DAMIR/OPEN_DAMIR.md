# OPEN_DAMIR

OPEN DAMIR (Dépenses de l'Assurance Maladie Inter-Régimes)


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|AGE_BEN_SNDS|nombre réel|Tranche d'Age Bénéficiaire au moment des soins|||
|BEN_RES_REG|nombre réel|Région de Résidence du Bénéficiaire|||
|DDP_SPE_COD|nombre réel|Discipline de Prestation Etb Exécutant|||
|ETE_CAT_SNDS|nombre réel|Catégorie Etb Exécutant|||
|ETE_REG_COD|nombre réel|Région d'Implantation Etb Exécutant|||
|ETE_TYP_SNDS|nombre réel|Type Etb Exécutant|||
|ETP_CAT_SNDS|nombre réel|Catégorie Etb Prescripteur|||
|ETP_REG_COD|nombre réel|Région d'Implantation Etb Prescripteur|||
|EXE_INS_REG|nombre réel|Région du PS Exécutant|||
|MDT_TYP_COD|nombre réel|Mode de Traitement Etb Exécutant|||
|ORG_CLE_REG|nombre réel|Région de l'Organisme de Liquidation|||
|PRE_INS_REG|nombre réel|Région du PS Prescripteur|||
|PSE_ACT_CAT|nombre réel|Catégorie de l' Exécutant|||
|PSE_ACT_SNDS|nombre réel|Nature d'Activité PS Exécutant|||
|PSE_SPE_SNDS|nombre réel|Spécialité Médicale PS Exécutant|||
|PSE_STJ_SNDS|nombre réel|Statut Juridique PS Exécutant|||
|PSP_ACT_CAT|nombre réel|Catégorie du Prescripteur|||
|PSP_ACT_SNDS|nombre réel|Nature d'Activité PS Prescripteur|||
|PSP_SPE_SNDS|nombre réel|Spécialité Médicale PS Prescripteur|||
|PSP_STJ_SNDS|nombre réel|Statut Juridique PS Prescripteur|||
|FLT_DEP_MNT|nombre réel|Montant du dépassement de la prestation|||
|PRS_REM_MNT|nombre réel|Montant remboursé|||
|MTM_NAT|nombre réel|Modulation du ticket modérateur|||
|EXO_MTF|nombre réel|Motif d'exonération du ticket modérateur|||
|TOP_PS5_TRG|nombre réel|Top PS5 Tous Régimes|||
|FLT_REM_MNT|nombre réel|Montant versé - remboursé (Part de base uniquement)|||
|ETE_IND_TAA|nombre réel|Indicateur TAA|||
|PRS_REM_TAU|nombre réel|Taux de remboursement réel (régime obligatoire)|||
|PRS_NAT|nombre réel|Nature de prestation|||
|MFT_COD|nombre réel|Code du mode de fixation des tarifs|||
|DRG_AFF_NAT|chaîne de caractères|Code affiné du destinataire du règlement|||
|FLT_ACT_NBR|nombre réel|Dénombrement de la prestation|||
|BEN_SEX_COD|nombre réel|Code sexe du bénéficiaire|||
|PRS_ACT_COG|nombre réel|Coefficient global de l'acte de base|||
|FLT_PAI_MNT|nombre réel|Montant de la dépense de la prestation|||
|PRS_REM_BSE|nombre réel|Base de remboursement|||
|BEN_CMU_TOP|chaîne de caractères|Top bénéficiaire de la CMU complémentaire|||
|FLT_ACT_QTE|nombre réel|Quantité de la prestation|||
|PRS_PDS_QCP|nombre réel|Code qualificatif du parcours de soins|||
|FLT_ACT_COG|nombre réel|Coefficient Global de la prestation|||
|ASU_NAT|nombre réel|Nature de l'assurance|||
|PRS_ACT_QTE|nombre réel|Quantité de l'acte de base|||
|SOI_ANN|chaîne de caractères|Année de soin PS5|||
|CPT_ENV_TYP|nombre réel|Type d'enveloppe|||
|BEN_QLT_COD|nombre réel|Qualité du bénéficiaire|||
|PRS_FJH_TYP|nombre réel|Type de prise en charge Forfait Journalier|||
|PRS_DEP_MNT|nombre réel|Montant global du dépassement|||
|PRS_ACT_NBR|nombre réel|Dénombrement des actes de base|||
|SOI_MOI|chaîne de caractères|Mois de soin PS5|||
|CPL_COD|nombre réel|Majorations hors actes CCAM - complément dacte|||
|PRS_PAI_MNT|nombre réel|Montant global de la dépense|||
|PRS_REM_TYP|nombre réel|Type de remboursement|||
|FLX_ANN_MOI|chaîne de caractères|Année et Mois de traitement|||
|ATT_NAT|nombre réel|Nature de l'accident du travail|||
|PRS_PPU_SEC|nombre réel|Code privé - public de la Prestations|||
