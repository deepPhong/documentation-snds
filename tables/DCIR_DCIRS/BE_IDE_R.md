# BE_IDE_R

Référentiel CNAMTS des établissements

- Clé primaire : `IDE_ETA_NU8`

## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|IDE_ETA_NUM|chaîne de caractères|Numéro Finess de l'Etablissement|||
|IDE_ETA_NU8|chaîne de caractères|Numéro Finess de l'Etabt sans clé|||
|IDE_ETA_NOM|chaîne de caractères|Raison Sociale Abrégée|||
|IDE_IDE_CPL|chaîne de caractères|Complément d'identification|||
|IDE_VOI_NUM|chaîne de caractères|Numéro dans la voie|||
|IDE_VOI_CNU|chaîne de caractères|Complément Numéro de voie|||
|IDE_VOI_TYP_LRG|chaîne de caractères|Nature de la voie|||
|IDE_VOI_LIB|chaîne de caractères|Nom de la voie|||
|IDE_ADR_CPL|chaîne de caractères|Complément d'adresse|||
|IDE_RSD_LIB|chaîne de caractères|Nom de la localité ou bureau distributeur|||
|IDE_BDI_COD|chaîne de caractères|Code postal|||
|IDE_TEL_NU1|chaîne de caractères|Numéro téléphone1|||
|IDE_TEL_NU2|chaîne de caractères|Numéro téléphone2|||
|IDE_INT_ADR|chaîne de caractères|Adresse internet|||
|IDE_FAX_NUM|chaîne de caractères|Numéro télécopie|||
|IDE_CPT_RSC|chaîne de caractères|Raison sociale complète|||
|IDE_INS_COM|chaîne de caractères|Numéro de Département Numéro de Commune|||
|IDE_CPA_NUM|chaîne de caractères|Code Caisse de rattachement de l'Etablissement|||
|IDE_CRA_NUM|chaîne de caractères|Code CRAM de rattachement de l'Etablissement|||
|IDE_NUM_NOU|chaîne de caractères|Nouveau no Finess|||
|IDE_NUM_DTE|date|Date nouveau Finess|||
|IDE_PSH_CAT|chaîne de caractères|Code catégorie de l'Etablissement|||
|IDE_CCL_DTE|date|Date de classement de l'Etablissement|||
|IDE_PSH_STJ|chaîne de caractères|Statut Juridique de l'Etablissement|||
|IDE_STJ_DTE|date|Date d'effet du Statut Juridique de l'Etablissement|||
|IDE_PSH_MFT|chaîne de caractères|Code MFT de l'Etablissement|||
|IDE_MFT_DTE|date|Date d'effet du MFT de l'Etablissement|||
|IDE_HON_TYP|chaîne de caractères|Code Type d'Honoraire de l'Etablissement|||
|IDE_HON_DTE|date|Date d'effet du Type d'Honoraire de l'Etablissement|||
|IDE_GES_NUM|chaîne de caractères|Numéro Finess Gestionnaire|||
|IDE_GES_DTE|date|Date d'effet du rattachement|||
|IDE_SRT_NUM|chaîne de caractères|Numéro SIRET|||
|IDE_ETA_DTE|date|Date d'effet du Code Activité de l'Etablissement|||
|IDE_ATI_COD|chaîne de caractères|Code Activité de l'Etablissement|||
|IDE_PSP_NAT|chaîne de caractères|Code PSPH de l'Etablissement|||
|IDE_PSP_DTE|date|Date d'effet du PSPH de l'Etablissement|||
|IDE_BGL_PEX|chaîne de caractères|Premier exercice de fonctionnement Bugdet Global de l'Etablissement|||
|IDE_ETA_TYP|chaîne de caractères|Code type de l'Etablissement|||
|IDE_COD_A24|chaîne de caractères|Code annexe 24 principal|||
|IDE_SAN_PUB|chaîne de caractères|Code structure secteur sanitaire public|||
|IDE_IDE_EAM|nombre entier|Code AM de l'Etablissement|||
|IDE_IDE_DAM|date|Date de Début de l'AM de l'Etablissement|||
|IDE_IDE_FAM|date|Date de Fin de l'AM de l'Etablissement|||
|IDE_IDE_EAS|nombre entier|Code AS de l'Etablissement|||
|IDE_IDE_DAS|date|Date de Début de l'AS de l'Etablissement|||
|IDE_IDE_FAS|date|Date de Fin de l'AS de l'Etablissement|||
|IDE_IDE_CON|nombre entier|Code contrat de l'Etablissement|||
|IDE_IDE_DON|date|Date de Début du contrat de l'Etablissement|||
|IDE_IDE_FON|date|Date de Fin du contrat de l'Etablissement|||
|IDE_NUM_ANC|chaîne de caractères|No Finess ancien|||
|IDE_ANC_DTE|date|Date Finess ancien|||
|IDE_IDE_CTR|nombre réel|Code convention tripartite|||
|IDE_IDE_DTR|date|Date début convention tripartite|||
|IDE_IDE_FTR|date|Date fin convention tripartite|||
|IDE_A24_24|chaîne de caractères|Code annexe 24|||
|IDE_A24_24B|chaîne de caractères|Code annexe 24B|||
|IDE_A24_24C|chaîne de caractères|Code annexe 24C|||
|IDE_A24_24Q|chaîne de caractères|Code annexe 24Q|||
|IDE_A24_24S|chaîne de caractères|Sans annexe 24|||
|IDE_A24_24T|chaîne de caractères|Code annexe 24T|||
|IDE_NUM_PCP|chaîne de caractères|Code établissement principal|||
|IDE_CBU_CTR|nombre réel|Code contrat médicaments|||
|IDE_CBU_EFF|date|Date effet contrat médicaments|||
|IDE_CBU_TAU|chaîne de caractères|Taux contrat médicaments|||
|IDE_CBU_DTF|date|Date fin contrat médicaments|||
|IDE_FIN_IND|chaîne de caractères|Indice Financement|||
|IDE_CAI_PIV|chaîne de caractères|Caisse Pivot|||
|IDE_NAT_ORI|chaîne de caractères|Code Nature de l'Origine de l'Etablissement (CPAM ou CRAM)|||
|IDE_MCO_COE|nombre réel|Coefficient MCO|||
|IDE_COE_HAD|nombre réel|Coefficient HAD|||
|IDE_MCO_DTE|date|Date D'effet du Coefficient MCO|||
|IDE_HAD_DTE|date|Date D'effet du Coefficient HAD|||
|IDE_IMP_DPT|chaîne de caractères|Département Implantation de l'Etablissement|||
